import json
import sys
import argparse

import requests
import toml
from requests.structures import CaseInsensitiveDict

HEADERS = CaseInsensitiveDict()
HEADERS["Content-Type"] = "application/x-www-form-urlencoded"

HEADER = "\033[95m"
OKBLUE = "\033[94m"
OKCYAN = "\033[96m"
OKGREEN = "\033[92m"
WARNING = "\033[93m"
FAIL = "\033[91m"
ENDC = "\033[0m"
BOLD = "\033[1m"
UNDERLINE = "\033[4m"


def get_json(url):
    try:
        r = requests.get(url)
    except Exception as e:
        print(e)
        sys.exit(1)
    return r.json()


def post_json(url, json):
    print(f"Sent {OKBLUE}{json}{ENDC} to {OKGREEN}{url}{ENDC}.")
    try:
        r = requests.post(url, headers=HEADERS, data=f"json={json}")
    except Exception as e:
        print(e)
        sys.exit(1)
    return r.status_code


parser = argparse.ArgumentParser(description="AVB auto configuration tool")
parser.add_argument(
    "-c",
    "--config",
    type=str,
    required=True,
    default="mu",
    action="store",
    help="Configuration file name",
)
args = parser.parse_args()

if __name__ == "__main__":
    config = toml.load(f"config/{args.config}.toml")
    devices = config["devices"]
    base = "http://primary.local/datastore"

    # step 1 -- config the RME Digiface AVB
    uid_rme = config["rme"]["uid"]
    dict_rme = {
        f"avb/{uid_rme}/cfg/0/input_streams/{idx}/talker": f"{talker}"
        for idx, talker in enumerate(config["rme"]["talker"])
    }
    post_json(base, json=json.dumps(dict_rme))

    # step 1.1 -- config the clock master
    uid_clock = config["rme"]["talker"][0][:-2]
    for dev in devices:
        base = f"http://{dev}.local/datastore"
        uid_motu = config[dev]["uid"]
        clock_source = get_json(f"{base}/avb/{uid_motu}/cfg/0/clock_sources")
        if uid_motu == uid_clock:
            for clk_idx in range(int(clock_source["num"])):
                if clock_source[f"{clk_idx}/object_name"] == "Internal":
                    dict_clock = {
                        f"avb/{uid_motu}/cfg/0/clock_source_index": str(clk_idx)
                    }
                    post_json(base, json=json.dumps(dict_clock))
        else:
            for clk_idx in range(int(clock_source["num"])):
                if clock_source[f"{clk_idx}/object_name"] == "AVB Input Stream 1":
                    dict_clock = {
                        f"avb/{uid_motu}/cfg/0/clock_source_index": str(clk_idx)
                    }
                    post_json(base, json=json.dumps(dict_clock))

    # step 2 -- config the MOTU 8pre devices
    for dev in devices:
        base = f"http://{dev}.local/datastore"
        uid_motu = config[dev]["uid"]

        # step 2.1 config the talker
        dict_dev = {
            f"avb/{uid_motu}/cfg/0/input_streams/0/talker": config[dev]["talker"][0]
        }
        post_json(base, json=json.dumps(dict_dev))

        # step 2.2 config the mic and tweeter gain
        assert len(config[dev]["mic_channel"]) == len(config[dev]["mic_gain"])
        assert len(config[dev]["twt_channel"]) == len(config[dev]["twt_gain"])

        # mic
        dict_dev = {"ext/ibank/1/userCh": str(len(config[dev]["mic_channel"]))}
        for mic in config[dev]["mic_channel"]:
            dict_dev[f"ext/ibank/1/ch/{mic}/trim"] = config[dev]["mic_gain"][mic]
            dict_dev[f"ext/ibank/1/ch/{mic}/48V"] = "1"

        # tweeter
        dict_dev["ext/obank/1/userCh"] = str(len(config[dev]["twt_channel"]))
        for twt in config[dev]["twt_channel"]:
            dict_dev[f"ext/obank/1/ch/{twt}/trim"] = config[dev]["twt_gain"][twt]
        post_json(base, json=json.dumps(dict_dev))
